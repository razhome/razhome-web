// server.js
const express = require('express');
const next = require('next');
const bodyParser = require('body-parser');

const getPort = require('./util/get-port');
const PORT = getPort(process.env.PORT || 3000);
const dev = process.env.NODE_ENV !== "dev";         // Checks if running a dev environment
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();
const mongoose = require('mongoose');
const mongoCreds = require('./creds/mongo.json');   // Info about mongo database

// Connects to the MongoDB. Though db is never used, the connection is accessable globally
const db = mongoose.connect("mongodb+srv://" + mongoCreds.username + ":" + mongoCreds.password + mongoCreds.uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: "packages"
});

nextApp.prepare().then(() => {
    const app = express();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    // map endpoints
    app.use('/api/packs', require('./api/routes/packs'));
    app.use('/api/status', require('./api/routes/pi-status'));
    app.use('/api/voice', require('./api/routes/voicebindings'));
    app.use('/api/azure', require('./api/routes/azure'));

    app.get('*', (req, res) => {
        return (handle(req, res));
    });
    app.set('port', PORT);
    

    app.listen(PORT, (err) => {
        if (err) throw err;
        console.log(`ready on port ${PORT}`);
    });

    
})
