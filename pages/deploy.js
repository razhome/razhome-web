// pages/deploy.js

// Frontend dependancies
import React from 'react';
import Layout from '../components/global/Layout';
import OutputConsole from '../components/deploy/OutputConsole';
import ReportDisplay from '../components/deploy/ReportDisplay';
import PackageInfo from '../components/packages/PackageInfo';
import ProgressButton from '../components/deploy/ProgressButton';
import './deploy.scss';

// Code dependancies
const validatePack = require("../util/validate-pack-browser.js");
const apiUrl = require('../api/api-url');
import axios from 'axios';

class ExternButton extends React.Component {
    constructor(props) {
        super();
        this.state = {
            ready: props.disabled
        };

        this.setReady = (state) => {
            this.setState({ ready: state});
        }
    }

    render() {
        return (
            <button onClick={this.props.onClick} disabled={!this.state.ready}>Deploy To Razhome</button>
        );
    }
}

class Deploy extends React.Component {

    constructor() {
        super();
        this.consoleRef = React.createRef();
        this.fileRef = React.createRef();
        this.reportRef = React.createRef();
        this.buttonRef = React.createRef();
        this.packageRef = React.createRef();
        this.report = null;

        // Methods
        this.validatePackage = (file) => {
            let logCb = (text) => { this.consoleRef.current.consoleLog(text); };
            file.text().then((tarstring) => {
                validatePack(tarstring, file.name, logCb, (report) => {
                    console.log(report);
                    this.report = report;
                    this.reportRef.current.setImage(report.code);
                    this.reportRef.current.setStatus("Validation Completed");
                    this.reportRef.current.setContent(report.summary);
                    if(report.warnings.length != 0) {
                        logCb("Warnings: ")
                        report.warnings.forEach((val) => logCb(val));
                    }
                    if(report.errors.length != 0) {
                        logCb("Errors: ")
                        report.errors.forEach((val) => logCb(val));
                    }
                    if(report.code < 3) {
                        this.buttonRef.current.setMode(1);
                        this.packageRef.current.setPackage(report.packfile);
                    }
                });
            }).catch((err) => {
                logCb("An error occured. Check browser console for details.");
                console.log(err);
            })
        }

        this.handleSelect = (event) => {
            console.log(event);
            this.buttonRef.current.setMode(0);
            this.reportRef.current.reset();
            if(this.fileRef.current.files[0]) {
                this.validatePackage(this.fileRef.current.files[0]);
            }
        }

        this.submitFile = (event) => {
            var formData = new FormData();
            formData.append('namespace', this.report.packfile.namespace);
            formData.append('package', this.fileRef.current.files[0]);
            var config = {
                onUploadProgress: (progressEvent) => {
                    this.buttonRef.current.setProgress(Math.round((progressEvent.loaded * 100) / progressEvent.total));
                }
            }
            this.buttonRef.current.setMode(2);
            axios.post(apiUrl(document).uriMinusPath + "/api/azure", formData, config).then((res) => {
                this.buttonRef.current.setMode(3);
            });
        }
    }

    render() {
        return (
            <Layout>
                <div className="DeployPage">
                    <div className="OtherInfo">
                        <h1>How to Deploy</h1>
                        <p>
                            To begin, select "Choose File" down below and select your package file in the dialogue that comes up. 
                            Make sure that you generated your package with <a href="https://gitlab.com/razhome/razhome-pack">razhome-pack</a>
                            {" "} as it performs checks that are not performed in browser.
                        </p>
                    </div>
                    <div className="Package">
                        <PackageInfo ref={this.packageRef}/>
                    </div>

                    <div className="Report">
                        <ReportDisplay ref={this.reportRef} fileRef={this.fileRef} fileHandler={this.handleSelect}/>
                    </div>
                    <div className="Console">
                        <OutputConsole ref={this.consoleRef}/>
                    </div>
                    <div className="DeployButton">
                        {/* <button ref={this.buttonRef} onClick={this.submitFile} disabled={!this.state.ready}>Deploy To Razhome</button> */}
                        <ProgressButton ref={this.buttonRef} onClick={this.submitFile}>Deploy to RazHome</ProgressButton>
                    </div>
                </div>
            </Layout>
        )
    }
}

export default Deploy;