// pages/packs.js

import Layout from "../components/global/Layout";
import React from 'react';
const apiUrl = require('../api/api-url');
import fetch from 'isomorphic-unfetch'
import { v4 as uuidv4 } from 'uuid';

class Packs extends React.Component {
    constructor() {
        super();
        this.state = { packs : false }
    }

    componentDidMount() {
        if(!this.state.packs) {
            fetch(apiUrl(document).uriMinusPath + "/api/packs").then((res) => {
                res.json().then((packs) => {
                    console.log(packs);
                    this.setState({ packs })
                })
            }).catch((err) => {
                console.log(err);
            })
        }
    }

    render() {
        if(this.state.packs) {
            return (
                <Layout>
                    {this.state.packs.map((pack) => 
                        <div key={uuidv4()}><p>{pack.namespace}</p></div>
                    )}
                </Layout>
            );
        } else {
            return (
                <Layout>
                    <p>Loading...</p>
                </Layout>
            );
        }

    };
}

export default Packs;