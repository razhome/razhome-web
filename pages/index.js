import Layout from "../components/global/Layout";
import React from 'react';
import Router from 'next/router';
import mongoose from 'mongoose';

class Index extends React.Component {    
    constructor() {
        super();
        this.count = 0;
        this.state = {
            count: this.count
        };
    }

    inc = () => {
        this.count++;
        this.setState({
            count: this.count
        });
    }

    render() {
        console.log(mongoose.models);
        return (
            <Layout>
                <p>{this.state.count} Rendering in a Class</p>
                <button onClick={this.inc}>Increment </button>
            </Layout>

        );
    }
};


export default Index;