// components/deploy/ReportDisplay.js
import React from 'react';
import LoadingImg from "../../img/loading-512.png";
import SuccessImg from "../../img/success-512.png";
import WarningImg from "../../img/warning-512.png";
import ErrorImg from "../../img/error-512.png";
import "./ReportDisplay.scss";

class ReportDisplay extends React.Component {
    constructor() {
        super();
        
        this.inputRef = new React.createRef();
        this.buttonRef = new React.createRef();

        this.images = [ LoadingImg, SuccessImg, WarningImg, ErrorImg ];

        this.state = {
            imageIndex: 0,
            status: "No Package Loaded",
            content: "Load a package file to begin the deployment process."
        };

        this.setImage = (index) => {
            this.setState({
                imageIndex: index
            });
        };

        this.setStatus = (status) => {
            this.setState({
                status: status
            });
        };

        this.setContent = (content) => {
            this.setState({
                content: content
            });
        };

        this.reset = () => {
            this.setState({
                imageIndex: 0,
                status: "No Package Loaded",
                content: "Load a package file to begin the deployment process."
            });
        }
    }

    render() {
        return (
            <div className="Report">
                <div className="ReportHeader"><h4>Current Status: {this.state.status}</h4></div>
                <img src={this.images[this.state.imageIndex]} alt="Status" className="StatusImg"/>
                <div className="ReportContent">
                    <i>{this.state.content}</i>
                </div>
                <input type="file" ref={this.props.fileRef} onChange={this.props.fileHandler} accept=".rpkg" className="InputFile"/>
            </div>
        );
    }
}

export default ReportDisplay;