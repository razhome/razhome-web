import React from "react";
import './ProgressButton.scss';

class ProgressButton extends React.Component {
    constructor(props) {
        super();
        this.state = {
            mode: 0,
            progress: 0,
        };

        // Methods
        this.setMode = (mode) => {
            this.setState({ mode });
        }

        this.setProgress = (progress) => {
            this.setState({ progress });
        }

        this.getText = () => {
            if (this.state.mode == 2) {
                return `Progress: ${this.state.progress}%`;
            } else if (this.state.mode == 3) {
                return "Successfully Uploaded Package";
            } else {
                return this.props.children;
            }
        }

        this.getClassName = () => {
            if (this.state.mode == 2) {
                return "LoadingBar";
            } else if(this.state.mode == 3) {
                return "FinishedLabel"
            } else {
                return "ProgressButton";
            }
        }

        this.getLoadingStyle = () => {
            return { width: `${this.state.progress}%` }
        }
    }

    render() {
        return (
            <div className={this.getClassName()}>
                <button onClick={this.props.onClick} disabled={this.state.mode == 0 || this.state.mode >= 2}>{this.getText()}</button>
                <div className="progress" style={this.getLoadingStyle()} />
                <div className="background" />
            </div>
        );
    }
}

export default ProgressButton;