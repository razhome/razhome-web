// components/deploy/OutputConsole.js

import React from 'react';
import './OutputConsole.scss'

class OutputConsole extends React.Component {
    constructor(props) {
        super(props);

        this.textRef = React.createRef();

        this.consoleLog = (text) => {
            this.textRef.current.value += '\n' + text;
            this.textRef.current.scrollTop = this.textRef.current.scrollHeight;
        }
    }

    shouldComponentUpdate() {
        return false;
    }

    render() {
        return (
            <div className="Console">
                <textarea
                    readOnly
                    ref={this.textRef}
                    rows="8"
                    value="Awaiting Package..."
                />
            </div>
        );
    }
}

export default OutputConsole;