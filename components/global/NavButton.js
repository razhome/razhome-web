// components/global/NavButton.js
//import Router from "next/router";
import Link from 'next/link';
import './NavButton.scss';
import { v4 as uuidv4 } from 'uuid';

const NavButton = props => (
    <div key={uuidv4()} className="NavButton">
        <Link href={props.href}>
            <div key={uuidv4()} className="Label"><a>{props.text}</a></div>
        </Link>
    </div>
);

export default NavButton;