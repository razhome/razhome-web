// components/global/Header.js
import "./Header.scss";
import LogoFull from '../../img/LogoFull.png';
import Link from "next/link";
import PiStatus from './PiStatus';
import { v4 as uuidv4 } from 'uuid';

const Header = props => (
    <div key={uuidv4()} className="Header">
        <Link href="/">
            <a><img src={LogoFull} className="Logo"/></a>
        </Link>
        <div className="Status">
            <PiStatus/>
        </div>
    </div>
)

export default Header;