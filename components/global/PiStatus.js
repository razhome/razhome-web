// components/global/PiStatus.js
import React from 'react';
import './PiStatus.scss';
import { v4 as uuidv4 } from 'uuid';
const apiUrl = require('../../api/api-url');
import fetch from 'isomorphic-unfetch'

var gStatus = null;

class PiStatus extends React.Component {
    
    constructor() {
        super()
        this.state = {
            status: gStatus
        }
    }

    componentDidMount() {
        if(!this.state.status) {
            fetch(apiUrl(document).uriMinusPath + '/api/status').then((res) => {
                res.json().then((status) => {
                    gStatus = status[0];
                    this.setState({
                        status: status[0]
                    });
                })
            }).catch((err) => {
                this.setState({
                    status : {
                        time: "ERROR",
                        ip: "ERROR",
                        status: "ERROR"
                    }
                });
            });
        }
    }

    componentDidUpdate() {
        if(gStatus && !this.state.status) {
            this.setState({ status: gStatus });
        }
    }

    render() {
        if(this.state.status) {
            return (
                <div key={uuidv4()} className="status">
                    <p>Most Recent Update: {this.state.status.time}<br/>
                    IP: {this.state.status.ip} Status: {this.state.status.status}</p>
                </div>
            );
        } else {
            return (
                <div key={uuidv4()} className="status">
                    <p>Loading status...</p>
                </div>
            )
        }
    }
}

export default PiStatus;