// components/global/Layout.js
import Header from "./Header";
import "./Layout.scss";
import NavBar from './NavBar';
import { v4 as uuidv4 } from 'uuid';

function getButtons() {
    return [
        { href: "/", text: "Home" },
        { href: "/about", text: "About" },
        { href: "/packs", text: "Packages" },
        { href: "/deploy", text: "Deploy"}
    ]
}

const Layout = props => (
    <div key={uuidv4()} className="Layout">
        <div key={uuidv4()} className="Heading">
            <Header/>
            <NavBar navButtons={getButtons()}/>
        </div>
        <div key={uuidv4()} className="Content">

            {props.children}
            <style jsx global> {`
                body {
                    margin: 0;
                    padding: 0;
                    background-color: #DDDDDD;
                }
            `}
            </style>
        </div>
    </div>
);

export default Layout;