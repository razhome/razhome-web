// components/global/NavBar.js

import './NavBar.scss';
import NavButton from './NavButton';
import { v4 as uuidv4 } from 'uuid';

const NavBar = props => (
    <div key={uuidv4()} className="NavBar">
        {props.navButtons.map(button => (
            <NavButton key={uuidv4()} href={button.href} text={button.text}/>
        ))}
    </div>
);

export default NavBar;