// components/packages/PackageInfo.js

import React from 'react';
import './PackageInfo.scss';
import { v4 as uuidv4 } from 'uuid';

class PackageInfo extends React.Component {
    constructor() {
        super();
        this.state = {
            package: null
        };

        this.setPackage = (pack) => {
            this.setState({ package: pack });
        }
    }

    render() {
        if (this.state.package) {
            console.log(this.state.package);
            return (
                <div className="PackageInfo">
                    <h1>Package Summary:</h1>
                    <div className="PackageContent">
                        <div className="Heading">
                            <h2>{this.state.package.name}<br /></h2>
                            <h3>{this.state.package.namespace}</h3>
                            <p>{this.state.package.description}</p>
                        </div>
                        <div className="Commands">
                            <h3>Commands</h3>
                            {this.state.package.commands.map((command) => (
                                <li key={uuidv4()}>
                                    {this.state.package.namespace}.<b>{command.name}</b>
                                    <ul>
                                        <li key={uuidv4()}><b>Description: </b><i>{command.description}</i></li>
                                        <li key={uuidv4()}><b>Phrases: </b>{command.phrases.join(', ')}</li>
                                    </ul>
                                </li>
                            ))}
                        </div>
                        <div className="Dependancies">
                            <h3>Dependancies</h3>
                            {(this.state.package.deps) ? (
                                <div>
                                    <li key={uuidv4()}><b>Apt:</b> <i>{(this.state.package.deps.apt) ? this.state.package.deps.apt.join(', ') : "None"}</i></li>
                                    <li key={uuidv4()}><b>Pips:</b> <i>{(this.state.package.deps.pip) ? this.state.package.deps.pip.join(', ') : "None"}</i></li>
                                </div>
                            ) : (
                                    <i>No Dependancies Listed</i>
                                )}
                        </div>
                    </div>

                </div>

            )
        } else {
            return (
                <div className="PackageInfo">
                    <h1>Package Summary:</h1>
                    <p>Upload/Select a package to view information about it</p>
                </div>
            );
        }
    }
}

export default PackageInfo;