// utils/validate-pack.js

const tar = require('tar');
const fs = require('fs');
const path = require('path');

const addFile = (entry, files) => {
    files.push(entry.path);
}

/**
 * Performs the 't' operation on the given tar file and returns an array of all values
 * 
 * @param {String} path
 * @param {function(String)} logCb a function which will be called to output user-friendly logging information
 * @returns {String[]}
 */
function listPackage(path, logCb) {
    logCb("Reading package contents");
    var files = [];
    try {
        tar.t({ file: path, sync: true, onentry: entry => addFile(entry, files) });
    } catch (err) {
        console.log("FATAL: Failed to read package.");
        return null;
    }
    logCb("Successfully read package.");
    return files;
}

/**
 * Reads the packfile from the given tar archive and sends the parsed json to the callback
 * 
 * @param {String} path 
 * @param {Boolean} hasPackfile
 * @param {function(String)} logCb a function which will be called to output user-friendly logging information
 * @param {function(String) => void} cb called when the packfile is parsed
 * @param {function(any) => void} onErr called in case of an error
 */
function getPackfile(path, hasPackfile, logCb, cb, onErr = null) {
    if(hasPackfile) {    
        try {
            logCb("Attempting to read packfile from package");
            var tarStream = fs.createReadStream(path).pipe(new tar.Parse({
                filter: (path, entry) => {
                    if (path == "pack.json") {
                        return true;
                    } else {
                        return false;
                    }
                },
                onentry: (entry) => {
                    entry.on('data', (data) => {
                        logCb("Successfully located packfile.");
                        cb(JSON.parse(data));
                    })
                }
            }));
            //var file = new FileReader().readAsArrayBuffer()
        } catch (err) {
            if (onErr) {
                onErr(err);
            } else {
                throw err;
            }
        }
    } else {
        cb(null);
    }
}

/**
 * Performs basic checks on a list of files contained in the package
 * 
 * @param {String[]} files
 * @param {function(String)} logCb a function which will be called to output user-friendly logging information
 * @returns {{hasPackfile: Boolean, hasCommands: Boolean, hasInstall: Boolean, hasExtraFiles: Boolean, installFiles: String[], commandFiles: String[], extraFiles: String[]}}
 */
function basicCheck(files, logCb) {
    logCb("Performing basic checks and sorting all entries in package.");
    var basicChecks = {
        hasPackfile: false,
        hasCommands: false,
        hasInstall: false,
        hasExtraFiles: false,
        installFiles: [],
        commandFiles: [],
        extraFiles: []
    }
    for (let i = 0; i < files.length; i++) {
        // Checks are done in this order because the packer will likely list directories
        // in reverse alphabetical order and packs.json will be last
        if (/^install\/.*/.test(files[i])) {
            basicChecks.hasInstall = true;
            basicChecks.installFiles.push(files[i]);
            continue;
        }
        if (/^commands\/.*/.test(files[i])) {
            basicChecks.hasCommands = true;
            basicChecks.commandFiles.push(files[i]);
            continue;
        }
        if (/pack\.json/.test(files[i])) {
            basicChecks.hasPackfile = true;
            continue;
        }
        if (!(/(^install\/.*|^commands\/.*|pack\.json)/.test(files[i]))) {
            basicChecks.hasExtraFiles = true;
            basicChecks.extraFiles.push(files[i]);
            continue;
        }
    }
    return basicChecks;
}

/**
 * 
 * @param {String[]} commandList a list of the name property of commands in the packfile
 * @param {String[]} commandFiles a list of files in the commands/ directory of the packfile
 * @param {function(String)} logCb a function which will be called to output user-friendly logging information
 * 
 * @returns {{failedCommands: String[], extraRootScripts: String[]}}
 */
function commandCheck(commandList, commandFiles, logCb) {
    logCb("Performing checks on package commands directory.");
    var commandChecks = {
        failedCommands: [],
        extraRootScripts: []
    }
    logCb("Filtering for relevant files");
    var relevantFiles = commandFiles.filter((val) => /^commands\/[^\/]*\.sh/.test(val));
    var passedFiles = [];
    logCb("Checking that all commands exist.");
    for (let i = 0; i < commandList.length; i++) {
        let fileIndex = relevantFiles.indexOf(`commands/${commandList[i]}.sh`);
        if (fileIndex != -1) {
            passedFiles.push(relevantFiles[fileIndex]);
        } else {
            commandChecks.failedCommands.push(commandList[i]);
        }
    }
    logCb("Checking for extra command files in commands/");
    relevantFiles.forEach((val) => {
        if (passedFiles.indexOf(val) == -1) {
            commandChecks.extraRootScripts.push(val);
        }
    })

    return commandChecks;
}

/**
 * Checks for the existence of the two optional install files in 
 * 
 * @param {String[]} installFiles
 * @param {function(String)} logCb a function which will be called to output user-friendly logging information
 * @returns {{installScript: Boolean, startupScript: Boolean}}
 */
function installCheck(installFiles, logCb) {
    logCb("Checking install directory for relevant files");
    return {
        installScript: installFiles.indexOf("install/install.sh") != -1,
        startupScript: installFiles.indexOf("install/startup.sh") != -1
    };
}

/**
 * 
 * @param {{basicChecks: {hasPackfile: Boolean, hasCommands: Boolean, hasInstall: Boolean, hasExtraFiles: Boolean, installFiles: String[], commandFiles: String[], extraFiles: String[]}, commandChecks: {failedCommands: String[], extraRootScripts: String[]}, installChecks: {installScript: Boolean, startupScript: Boolean}}} checks the check params generated during validatePack()
 * @param {any} packfile
 * @param {function(String)} logCb a function which will be called to output user-friendly logging information
 * @returns {{warnings: String[], errors: String[]}}
 */
function generateReport(checks, filename, packfile, logCb) {

    logCb(`Generating final report on ${filename}`);
    let report = {
        warnings: [],
        errors: [],
        packfile: null
    }
    if(!checks.basicChecks.hasPackfile) {
        report.errors.push(`ERROR: pack.json was not found in ${filename}. The pack cannot install.`);
    } else {
        report.packfile = packfile;
        if(checks.basicChecks.hasCommands) {
            if(checks.commandChecks.failedCommands.length > 0) {
                report.errors.push(`ERROR: ${checks.commandChecks.failedCommands.length} commands listed in pack.json could not be found in commands/, meaning there is no way to execute them.`);
                checks.commandChecks.failedCommands.forEach((val) => report.errors.push(`ERROR: commands/${val}.sh could not be found in ${filename}.`));
            }
            if(checks.commandChecks.extraRootScripts.length > 0) {
                report.warnings.push(`WARNING: ${checks.commandChecks.extraRootScripts.length} non-command shell scripts were found in the commands/ directory. For ease of debugging, put these extra shell scripts in subdirectories of commands/`);
                checks.commandChecks.extraRootScripts.forEach((val) => report.warnings.push(`WARNING: non-command script ${val} was found in commands/`))
            }
        } else {
            report.errors.push(`ERROR: Essential directory commands/ was not found in ${filename}.`);
        }
    }
    if(checks.basicChecks.hasInstall) {
        if(!(checks.installChecks.installScript || checks.installChecks.startupScript)) {
            report.warnings.push("WARNING: install/ exists, but does not contain install.sh or startup.sh. No files will run on install.");
        }
    }

    if(report.errors.length > 0) {
        report.summary = `Validation failed with ${report.errors.length} error(s) and ${report.warnings.length} warning(s)`;
        report.code = -1;
    } else {
        report.summary = `Validation succeeded with ${report.errors.length} error(s) and ${report.warnings.length} warning(s)`;
        report.code = (report.warnings.length == 0) ? 1 : 0;
    }
    return report;
}

/**
 * @param {String} packPath
 * @param {function(String)} logCb a function which will be called to output user-friendly logging information
 * @param {function(any)}
 */
function validatePack(packPath, logCb, onFinished) {
    logCb(`Beginning validation on ${packPath}`);
    var files = listPackage(packPath, logCb);
    if(!files) {
        return { errors: ["ERROR: Failed to read package."], warnings: [], packfile: null, code: -1, summary: "Validation failed due to an invalid package" };
    }

    var checks = { basicChecks: basicCheck(files, logCb) };
    if (checks.basicChecks.hasInstall) {
        checks.installChecks = installCheck(checks.basicChecks.installFiles, logCb);
    }

    getPackfile(packPath, checks.basicChecks.hasPackfile, logCb, (packfile) => {
        logCb("Getting list of commands from packfile.");
        commandList = [];
        packfile.commands.forEach((val) => commandList.push(val.name));
        
        if (checks.basicChecks.hasCommands && checks.basicChecks.hasPackfile) {
            checks.commandChecks = commandCheck(commandList, checks.basicChecks.commandFiles, logCb);
        }

        report = generateReport(checks, path.parse(packPath).base, packfile, logCb);
        logCb("Validation complete.");
        onFinished(report);
    });


}

export default listPackage;