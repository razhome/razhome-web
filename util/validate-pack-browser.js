// util/validate-pack.js

const tar = require('tar-stream');

// IO
/**
 * Performs all needed read operations on the tarball and returns
 * 
 * @param {String} input
 * @param {filename} name
 * @param {function(String)} logCb a function which will be called to output user-friendly logging information
 * @param {function({files: String[], name: String, packfile})} finishCb
 */
function readPackage(input, filename, logCb, finishCb) {
    logCb("Reading package contents:");

    var packageInfo = {
        files: [],
        name: filename,
        packfile: null
    };

    var extract = tar.extract();
    extract.on("entry", (header, stream, next) => {
        logCb("  -> " + header.name);
        packageInfo.files.push(header.name);

        stream.on('end', () => {
            next();
        });

        stream.on('data', (chunk) => {
            if (header.name == "pack.json") {
                packageInfo.packfile = JSON.parse(new TextDecoder("utf-8").decode(chunk));
            }
        })
    })

    extract.on('finish', () => {
        logCb("Successfully read package");
        finishCb(packageInfo);
    });

    extract.write(input);
    extract.end();

}

// Validation checks
/**
 * Performs basic checks on a list of files contained in the package and sorts the command and install files for further testing. 
 * The checks include:
 *  1. Are there files in install/
 *  2. Are there files in commands/
 *  3. Are there extra files in the root directory
 * The sorting breaks the all files besides 'packs.json' installFiles, commandFiles, and extraFiles, which should be self-explanitory
 * 
 * @param {String[]} files
 * @param {function(String)} logCb a function which will be called to output user-friendly logging information
 * @returns {{hasCommands: Boolean, hasInstall: Boolean, hasExtraFiles: Boolean, installFiles: String[], commandFiles: String[], extraFiles: String[]}}
 */
function basicChecks(files, logCb) {
    logCb("Performing basic checks and sorting all entries in package.");
    var basicChecks = {
        hasCommands: false,
        hasInstall: false,
        hasExtraFiles: false,
        installFiles: [],
        commandFiles: [],
        extraFiles: []
    }

    for (let i = 0; i < files.length; i++) {
        // Checks are done in this order because the packer will likely list directories
        // in reverse alphabetical order and packs.json will be last

        // checks for files in install/
        if (/^install\/.*/.test(files[i])) {
            basicChecks.hasInstall = true;
            basicChecks.installFiles.push(files[i]);
            continue;
        }
        // checks for files in commands/
        if (/^commands\/.*/.test(files[i])) {
            basicChecks.hasCommands = true;
            basicChecks.commandFiles.push(files[i]);
            continue;
        }
        // checks for files/folders in the root directory besides pack.json, install/, and commands/
        if (!(/(^install\/.*|^commands\/.*|pack\.json)/.test(files[i]))) {
            basicChecks.hasExtraFiles = true;
            basicChecks.extraFiles.push(files[i]);
            continue;
        }
    }
    logCb("Finished basic checks");
    return basicChecks;
}

/**
 * 
 * @param {String[]} commandList a list of the name property of commands in the packfile
 * @param {String[]} commandFiles a list of files in the commands/ directory of the packfile
 * @param {function(String)} logCb a function which will be called to output user-friendly logging information
 * 
 * @returns {{failedCommands: String[], extraRootScripts: String[]}}
 */
function commandChecks(commandList, commandFiles, logCb) {
    logCb("Performing checks on package commands directory.");
    var commandChecks = {
        failedCommands: [],
        extraRootScripts: []
    }
    logCb("Filtering for relevant files");
    var relevantFiles = commandFiles.filter((val) => /^commands\/[^\/]*\.sh/.test(val));
    var passedFiles = [];
    logCb("Checking that all commands exist.");
    for (let i = 0; i < commandList.length; i++) {
        let fileIndex = relevantFiles.indexOf(`commands/${commandList[i]}.sh`);
        if (fileIndex != -1) {
            passedFiles.push(relevantFiles[fileIndex]);
        } else {
            commandChecks.failedCommands.push(commandList[i]);
        }
    }
    logCb("Checking for extra command files in commands/");
    relevantFiles.forEach((val) => {
        if (passedFiles.indexOf(val) == -1) {
            commandChecks.extraRootScripts.push(val);
        }
    })

    return commandChecks;
}

/**
 * Checks for the existence of the two optional install files in 
 * 
 * @param {String[]} installFiles
 * @param {function(String)} logCb a function which will be called to output user-friendly logging information
 * @returns {{installScript: Boolean, startupScript: Boolean}}
 */
function installChecks(installFiles, logCb) {
    logCb("Checking install directory for relevant files");
    return {
        installScript: installFiles.indexOf("install/install.sh") != -1,
        startupScript: installFiles.indexOf("install/startup.sh") != -1
    };
}

// Reporting
/**
 * Generates a summery and result code based on the given report. Adds the fields code: Number and summary: String
 * 
 * @param {{ warnings: String[], errors: [], packfile: any }} report the finalized report from generateReport()
 */
function summarize(report) {
    if (report.errors.length != 0) {
        report.code = 3;
        report.summary = `Validation failed with ${report.errors.length} error(s) and ${report.warnings.length} warning(s). Package deployment cannot continue.`;
    } else {
        report.code = (report.warnings.length == 0) ? 1 : 2;
        report.summary = `Validation succeeded with ${report.errors.length} error(s) and ${report.warnings.length} warning(s). You may now deploy your package`;
    }
}

/**
 * Generates an error/warning report containi
 * 
 * @param {{basicChecks: {hasCommands: Boolean, hasInstall: Boolean, hasExtraFiles: Boolean, installFiles: String[], commandFiles: String[], extraFiles: String[]}, commandChecks: {failedCommands: String[], extraRootScripts: String[]}, installChecks: {installScript: Boolean, startupScript: Boolean}}} checks the check params generated during validatePack()
 * @param {function(String)} logCb a function which will be called to output user-friendly logging information
 * @returns {{warnings: String[], errors: String[], code: Number, summary: String, packfile: any}}
 */
function generateReport(packageInfo, checks = null) {
    var report = {
        warnings: [],
        errors: [],
        packfile: null
    };
    if (!packageInfo.packfile) {
        report.errors.push(`ERROR: pack.json not found in ${packageInfo.name}. Validation cannot continue.`);
        summarize(report);
        return report
    }
    report.packfile = packageInfo.packfile;
    if (checks.basicChecks.hasCommands) {
        if (checks.commandChecks.failedCommands.length > 0) {
            report.errors.push(`ERROR: ${checks.commandChecks.failedCommands.length} commands listed in pack.json could not be found in commands/, meaning there is no way to execute them.`);
            checks.commandChecks.failedCommands.forEach((val) => report.errors.push(`ERROR: commands/${val}.sh could not be found in ${packageInfo.name}.`));
        }
        if (checks.commandChecks.extraRootScripts.length > 0) {
            report.warnings.push(`WARNING: ${checks.commandChecks.extraRootScripts.length} non-command shell scripts were found in the commands/ directory. For ease of debugging, put these extra shell scripts in subdirectories of commands/`);
            checks.commandChecks.extraRootScripts.forEach((val) => report.warnings.push(`WARNING: non-command script ${val} was found in commands/`))
        }
    } else {
        report.errors.push(`ERROR: Essential directory commands/ was not found in ${packageInfo.name}.`);
    }
    if(checks.basicChecks.hasInstall) {
        if(!(checks.installChecks.installScript || checks.installChecks.startupScript)) {
            report.warnings.push("WARNING: install/ exists, but does not contain install.sh or startup.sh. No files will run on install.");
        }
    }
    if(checks.basicChecks.hasExtraFiles) {
        report.warnings.push(`WARNING: Found ${checks.basicChecks.extraFiles.length} in the root of the package other than pack.json. This may interfere with installation.`);
    }

    summarize(report);
    return report;
}

/**
 * 
 * @param {String} input The contents of the file stored in a string. 
 * @param {String} filename the name of the file
 * @param {function(String)} logCb Logging function. Expect a newline after each call
 */
function validatePackage(input, filename, logCb, finishCb) {
    logCb(`Beginning validation on ${filename}`)
    var checks = {};

    readPackage(input, filename, logCb, (packageInfo) => {
        if (!packageInfo.packfile) {
            logCb("Validation attempt completed");
            logCb("Generating report");
            let report = generateReport(packageInfo);
            finishCb(report);
            return;
        }
        checks.basicChecks = basicChecks(packageInfo.files, logCb);
        if(checks.basicChecks.hasCommands) {
            logCb("Getting list of commands from packfile.");
            commandList = [];
            packageInfo.packfile.commands.forEach((val) => commandList.push(val.name));
            checks.commandChecks = commandChecks(commandList, checks.basicChecks.commandFiles, logCb);
        }
        if(checks.basicChecks.hasInstall) {
            checks.installChecks = installChecks(checks.basicChecks.installFiles, logCb);
        }
        logCb("Validation attempt completed");
        logCb("Generating report");
        let report = generateReport(packageInfo, checks);
        finishCb(report);
        return;
    })
}

module.exports = validatePackage;