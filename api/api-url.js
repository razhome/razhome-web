//api/api-url.js

const url = require('url');

const requestParser = (function(doc) {
    var href = doc.location.href;
    var urlObj = url.parse(href, true);
    
    uriMinusPath = (urlObj.port) ? urlObj.protocol + '//' + urlObj.hostname + ":" + urlObj.port : urlObj.protocol + '//' + urlObj.hostname

    return {
        href,
        urlObj,
        getQueryStringValue: (key) => {
            let value = ((urlObj && urlObj.query) && urlObj.query[key]) || null;
            return value;
        },
        uriMinusPath
    };
});

module.exports = requestParser;