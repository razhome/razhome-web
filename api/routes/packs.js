// api/routes/index.js

const express = require('express');
const router = express.Router();
const Packages = require('../models/packs');
const Voices = require('../models/voicebindings');
const packFunctions = require('../functions/packs');

/** simply gets all packages and returns the array */
router.get('/', (req, res) => {
    console.log("Received GET on /api/packs")
    packFunctions.getAllPacks((err, packages) => {
        if(err) {
            res.status(500).send("Error: Unable to retrieve packages");
        } else {
            res.json(packages);
        }
    });
});

/** Returns a package with a specific name if one exists */
router.get('/ns/:ns', (req, res) => {
    console.log("Received GET on /api/packs/ns/:ns");
    packFunctions.getPackByNamespace(req.params.ns, (err, package) => {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json(package);
        }
    })
})


/** Search by command full name (namespace.command) */
router.get('/command/:fullname', (req, res) => {
    console.log("Received GET on /api/packs/command/:fullname");
    packFunctions.getCommandByName(req.params.fullname, (err, package, commandName) => {
        if (err) {
            res.status(500).send(err);
        } else {
            var index = -1;
            for (var i = 0; i < package.commands.length; i++) {
                if (package.commands[i].name == commandName) {
                    index = i;
                    break;
                }
            }
            res.json({ index, package });
        }
    })
})

/** Pushes a package to the database and updates the voice command list with default bindings */
router.post('/', (req, res) => {
    console.log("Received POST on /api/packs");
    Packages.find({ namespace : req.body.namespace}).then((packs) => {
        var pack;
        if(packs.length != 0) {
            pack = packs[0];
        } else {
            pack = new Packages();
        }
        try {
            pack.name = req.body.name;
            pack.namespace = req.body.namespace;
            pack.description = req.body.description;
            req.body.commands.forEach(command => {
                pack.commands.push(command);
                command.phrases.forEach(phrase => {
                    let voicebind = new Voices({
                        phrase: phrase,
                        command: command.name,
                        namespace: pack.namespace
                    });
                    voicebind.save()
                        .then((doc) => {
                            res.json(doc);
                        })
                        .catch((err) => {
                            res.status(500).send(`Failed with error ${err}`);
                        });
                });
            });
            pack.save().then((doc) => {
                res.send("Successfully added packages to database");
            }).catch((err) => {
                res.status(500).send(`Failed with error ${err}`);
            });
        } catch (err) {
            res.status(400).send(`Invalid request. Command failed with error ${err}`);
        }
    }).catch((err) => {
        res.status(500).send(err);
    })
});

router.delete('/ns/:ns', (req, res) => {
    Packages.deleteOne({namespace: req.params.ns}, (err) => {
        if(err) {
            res.status(500).send(err);
        } else {
            res.send("Deleted package " + req.params.ns);
        }
    });
})


module.exports = router;