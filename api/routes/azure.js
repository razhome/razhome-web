// api/routes/azure.js

const express = require('express');
const router = express.Router();
const formidable = require('formidable');
const azureFunctions = require('../functions/azure');

// Azure Storage API calls
router.get('/', (req, res) => {
    console.log("Received GET on /api/azure");
    azureFunctions.listBlobs().then((blobs) => {
        res.json(blobs);
    }).catch((err) => {
        res.status(500).json(err);
    })
});

router.get("/download", (req, res) => {
    console.log("Received GET on /api/azure/download");
    if(req.query.name) {
        console.log(`Attempting to download blob ${req.query.name}`);
        azureFunctions.downloadBlob(req.query.name).then((dlRes) => {
            console.log(dlRes);
            res.download(dlRes.tmpFile.name, dlRes.filename, (err) => {
                if(err) {
                    console.log("Failed to transfer file to host");
                    console.log(err);
                }
                dlRes.tmpFile.removeCallback();
            });
        }).catch((err) => {
            if(err.statusCode && err.statusCode == 404) {
                res.status(404).send("Blob not found");
            } else {
                res.status(500).json(err);
            }
        })
    } else {
        res.status(400).send("Error: No blob name provided");
    }
});

router.post('/', (req, res) => {
    console.log("Received POST on /api/azure")
    new formidable.IncomingForm().parse(req, (err, fields, files) => {
        console.log(fields);

        azureFunctions.pushBlobToAzure(files.package).then((blobName) => {
            azureFunctions.mongoQueueBlob(blobName).then((doc) => {
                res.json({message: "Succeeded in upload and queued for installation", success: true});
            }).catch(reason => {
                res.json({message: "Succeeded in uploaded, but failed to queue for installation", success: false, reason});
            });
        })
    });
});

// MongoDB API calls
router.get('/blobs', (req, res) => {
    azureFunctions.mongoGetBlobs().then(blobs => {
        res.json(blobs);
    }).catch(err => {
        res.status(500).json(err);
    })
});

router.post('/blobs/queue', (req, res) => {
    if(req.query.name) {
        azureFunctions.mongoQueueBlob(req.query.name).then(doc => {
            res.json(doc);
        }).catch(err => {
            res.status(500).json(err)
        });
    } else {
        res.status(400).send("Invalid query. 'name' not provided");
    }
});

router.delete('/blobs/dequeue', (req, res) => {
    azureFunctions.mongoDequeueBlob().then((blob) => {
        res.send({ success: true, blob });
    }).catch((errInfo) => {
        errInfo.err.success = false;
        res.status(errInfo.status).json(errInfo.err);
    });
});

module.exports = router;