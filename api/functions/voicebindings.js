// api/functions/voicebindings.js

const VoiceModel = require('../models/voicebindings');

const getAllPhrases = async () => {
    let phrases = await VoiceModel.find({}, (err, docs) => {
        if (err) throw err;
    }).exec()
    return phrases;
}

module.exports = { getAllPhrases };