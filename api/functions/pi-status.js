// api/functions/pi-status.js

const PiStatus = require('../models/pi-status');

/** 
 * Gets the most recent status
 * 
 * @param {function(any Document[])} cb 
 */
const getRecentStatus = (cb) => {
    PiStatus.find().sort({"time" : -1 }).limit(1).exec((err, doc) => {
        cb(err, doc);
    });
}

module.exports = { getRecentStatus };