const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AzureBlobSchema = new Schema({
    name: String,
    added: { type: Date, default: Date.now }
});

module.exports = mongoose.model("AzureBlob", AzureBlobSchema);