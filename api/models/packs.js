// api/models/packs.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PackageSchema = new Schema({
    name: String,
    namespace: String,
    description: String,
    commands:[{name: String, phrases: [String], description: String}]
});

module.exports = mongoose.models["package"] || mongoose.model("package", PackageSchema);