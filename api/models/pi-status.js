// api/models/pi-status.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PiStatusSchema = new Schema({
    time: { type: Date, default: Date.now },
    status: String,
    ip: String
});

function getModel() {
    if(mongoose.models) {
        return mongoose.models["pi-status"];
    } else {
        return false;
    }
}

module.exports = getModel() || mongoose.model("pi-status", PiStatusSchema);